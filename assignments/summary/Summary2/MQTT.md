# **<div align="center">MQTT</div>**
                
![](https://www.electronicwings.com/public/images/user_images/images/NodeMCU/NodeMCU%20Basics%20using%20ESPlorer%20IDE/NodeMCU%20MQTT%20Client/MQTT%20Broker%20nw.png)

## MQTT protocol terms:

### **Client**

### MQTT consist of two kinds of clients communicating with a server/broker

- A client who sends data to broker is called as Publisher.
- A client who receives data from broker is called as Subscriber.

### **MQTT broker**

- It is an application which you need to install on your server device which is responsible for receiving data from Publishers and delivers to subscribers.

### **Topic(-t)**

- The topic is like a Communication channel on which Publisher publishes data and Subscriber subscribes data.

### **Host Name(-h)**

- Host name is an IP address of machine or computer on which broker is Installed.

### **Port(-p)**

- Port is used to identify a service. Default port for MQTT is 1883.

### Keepalive(-k)

- Duration after which subscriber sending PING request to broker again and again to maintain a connection,  Default keep alive Duration is 60 sec.

### Payload

- The payload is nothing but actual the data which is to be sent from one place to another place. The broker can receive the maximum payload size of 268435455 bytes.

### Qos(-q)

- In Qos0 the message is sent only once and there is no check of whether data is received/delivered or not (fire and forget).
- In Qos1 the message is send by the sender again and again until acknowledgement is received (acknowledged delivery).Here is  a chances of having duplicate message when message is received but acknowledgement is lost.
- In Qos2 the sender and receiver engage in a two-level handshake to ensure only one copy of the message is received (assured delivery).
