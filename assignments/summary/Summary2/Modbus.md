# **Modbus**

![](https://i1.wp.com/media.visaya.solutions/2018/09/1.png?resize=528%2C317)

# **Modbus Communication**

![](https://www.rtautomation.com/wp-content/uploads/2018/09/modbusrtu_diagram.jpg)

## **Modbus RTU vs Modbus TCP**

## Modbus RTU

- Only one set of signals can be on the RS485 link at any one time. Either the single RTU Master is transmitting or one of the RTU Client devices is transmitting.

-  Devices with old 8-bit processors and a tiny bit of memory can easily do Modbus serial

-  RS485 had an electrical limitation of 32 devices

## Modbus TCP


- With Modbus TCP, controllers can much more efficiently use the bandwidth on Ethernet to be the Master to hundreds of Modbus TCP devices

- Need a more expensive platform to do Ethernet.

- allows a very large number of concurrent connections