# **Shunya Interfaces**

## **Microprocessor Interfaces programming**

Shunya Interfaces Core API
Shunya Interfaces depends on the core library to provide basic API's to Interact
with the Microprocessor Interfaces like GPIO, SPI, UART, I2C.
While porting sensors Use these API's for interacting with sensors.

## **GPIO API**
| API    | Description | Details|
| ------ | ------ | ------ |
|`pinmode()`| Sets the direction of the GPIO pin to INPUT or OUTPUT| [Read more](#pinmode())|
| `digitalWrite()` | Sets the digital value of the GPIO pin |[Read more](#digitalWrite) |
| `digitalRead()` | Read digital value of the GPIO pin | [Read more](#digitalRead)|



## **Interrupt API**
| API    | Description | Details|
| ------ | ------ | ------ |
| `attachInterrupt()` | Sets the digital value of the GPIO pin. | [Read more](#interrupt)|


## **Delay API**
| API    | Description | Details|
| ------ | ------ | ------ |
| `delay()` | Code sleeps for given milliseconds | [Read more](#delay)|
| `delayMicroseconds()` | Code sleeps for given microseconds | [Read More](#delayMicroseconds)|


## **I2C API**
| API    | Description | Details|
| ------ | ------ | ------ |
| `wireBegin()` | Initializes the I2C device |[Read More](#wireBegin)|
| `wireBeginTransmission()` | Starts I2C communication |[Read More](#wireBeginTransmission)|
| `wireWrite()` | Send data to the I2C device | [Read More](#wireWrite)|
| `wireRequestFrom()` | Request Data from the I2C device | [Read More](#wireRequestFrom)|
| `wireAvailable()` | Check number of bytes available to read from the device |[Read More](#wireAvailable)|
| `wireRead()` | Read from the I2C device | [Read More](#wireRead)|
| `wireEndTransmission()` | End communication to the I2C device | [Read More](#wireEndTransmission)|
| `wireEnd()` | De-initialize the I2C device | [Read More](#wireEnd)|

## **SPI API**
| API    | Description | Details|
| ------ | ------ | ------ |
|`spiBegin()`|Initializes the SPI device|[Read More](#spiBegin)|
|`spiBeginTransaction()`|Starts SPI communication.|[Read More](#spiBeginTransaction)|
|`spiTransfer()`|Send data to the I2C device|[Read More](#spiTransfer)|
|`spiEndTransaction()`|End the SPI transaction|[Read More](#spiEndTransaction)|
|`spiEnd()`|End the SPI communication|[Read More](#spiEnd)|
|`spiSetBitOrder()`|Set the bit order to MSB or LSB first|[Read More](#spiSetBitOrder)|
|`spiSetClock()`|Set SPI clock rate|[Read More](#spiSetClock)|
|`spiSetDataMode()`|Set SPI mode|[Read More](#spiSetDataMode)|

## **UART API**

| API    | Description | Details|
| ------ | ------ | ------ |
|`serialBegin()`|Open and initialize the serial port|[Read More](#serialBegin)|
|`serialEnd()`|Release the serial port|[Read More](#serialEnd)|
|`serialRead()`|Get a single character from the serial device|[Read More](#serialRead)|
|`serialPrint()`|Print over Serial|[Read More](#serialPrint)|
|`serialPrintln()`|Print over Serial and add a new line after every print|[Read More](#serialPrintln)|
|`serialWrite()`|Send a single character to the serial port|[Read More](#serialWrite)|
|`serialAvailable()`|Return the number of bytes of data available to be read in the serial port|[Read More](#serialAvailable)|
|`serialFlush()`|Flush the serial buffers (both tx & rx)|[Read More](#serialFlush)|


## **PWM API**

| API    | Description | Details|
| ------ | ------ | ------ |
|`softPwmBegin()`|Start the PWM at a initial duty cycle and a defined Pulse width|[Read More](#softPwmBegin)|
|`softPwmWrite()`|Change the PWM duty cycle|[Read More](#softPwmWrite)|
|`softPwmEnd()`|Stop the PWM signals on a pin|[Read More](#softPwmEnd)|


## GPIO API

### <a name="pinmode()"/>**`pinmode()`** </a>

**Description :** Sets the direction of the GPIO pin to INPUT or OUTPUT

**Parameters**

`physicalPin(int)` - Physical pin number of the GPIO

`mode(int)` - GPIO mode either INPUT or OUTPUT

**Return-type** : void

**Usage** : pinmode(7, OUTPUT);


### `digitalWrite()`

**Description :** Sets the digital value of the GPIO pin

**Parameters**

`physicalPin(int)` - Physical pin number of the GPIO

`value(int)` - value is either HIGH or LOW

**Return-type** : void

**Usage** : digitalWrite(7, HIGH);


### `digitalRead()`

**Description** : Read digital value of the GPIO pin

**Parameters**

`physicalPin(int)` - Physical pin number of the GPIO

**Return-type** : int

**Returns** : Return GPIO pin value on SUCCESS, Return -1 on FAILURE

**Usage** : result = digitalRead(7);


### **Delay API**

**<a name="delay"/>delay() </a>**

**Description** : Used to produce millisecond delays

**Parameters**

 **`howLong(unsigned int)`** - Delay in ms to be produced

**Return-type** : 

**Usage** : delay(50);


**<a name="delayMicroseconds"/>delayMicroseconds()</a>**

**Description**: Used to produce microsecond delays

**Parameters**


**`howLong(unsigned int)`** - Delay in us to be produced

**Return-type:** void

**Usage:** delayMicroseconds(50);


### **I2C/TWI API**

### `wireBegin()`

**Description :** Initializes the I2C device

**Parameters**

 **`bus(const int) `**- I2C device node

**Usage :** `wireBegin(1)`; //1 is the board I2C 1


### `wireBeginTransmission()`

**Description :** Starts I2C communication

**Parameters**

**`addr(const int)`** - I2C device address

**Return-type :** void

**Usage :** `wireBeginTransmission(0x23)`; //0x23 is the device address


### `wireWrite()`

**Description :** Send data to the I2C device

**Parameters**

 **`val(int)`** - Value to be written to the device.

**Return-type :** void

**Usage :** `wireWrite(1)`;


### `wireRequestFrom()`

**Description :** Request Data from the I2C device

**Parameters**

**`addr(int)`** - I2C address

 **`count(int)`** -  Number of Bytes to request

**Return-type :** int

**Returns :**  count of the data available  or returns -1 on FAILURE

**Usage :** `wireRequestFrom(0x23,5);` //0x23 is the address of the I2C device


### `wireAvailable()`

**Description :** Check number of bytes available to read from the device

**Return-type :** int

**Returns :**  count of the data available to read or returns -1 on FAILURE

**Usage :** ret = `wireAvailable();`

### `wireRead()`

**Description :** Read from the I2C device.

**Return-type :** int

**Returns :** -1 on FAILURE

**Usage :** val = `wireRead();`


### `wireEndTransmission()`

**Description :** End communication to the I2C device

**Return-type :** void

**Usage :** `wireEndTransmission();`


### `wireEnd()`

**Description :** De-initialize the I2C device

**Return-type :** void

**Returns :** -1 on FAILURE

**Usage :** `wireWriteReg8(fd,0x10,0x01);` //Write 0x01 in the register whose address is 0x10


### **SPI API**

### `spiBegin()`

**Description :** Initializes the SPI bus.

**Return-type :** void

**Usage :** `spiBegin();`


### `spiBeginTransaction()`

**Description :** Starts SPI communication.

**Parameters**

- clock       SPI clock in Hz
- bit_order   Bit order for SPI
- mode  SPI mode (can be 0,1,2,3)


**Return-type :** void

**Usage :** `spiBeginTransaction(50000, MSBFIRST, 0);`


### `spiTransfer()`

**Description :** Write 8 bits (1 byte) of data to the SPI bus.

**Parameters**


val Data to be written to the SPI device.

**Return-type :** Returns the Rx buffer of the SPI bus.

**Usage :** `spiTransfer(0x5D);`


### `spiEndTransaction()`

**Description :** Ends the SPI communication.

**Return-type :** void

**Usage :** `spiEndTransaction();`


### `spiEnd()`

**Description :** Ends the SPI device.

**Return-type :** void

**Usage :** `spiEnd();`


### `spiSetBitOrder()`

**Description :** Set the bit order to MSB or LSB first.

**Parameters**


**order:**       Bit order

**Return-type :** void

**Usage :** `spiSetBitOrder(MSBFIRST);`


### `spiSetClock()`

**Description :** Set SPI clock rate

**Parameters**


`max_speed`   Clock rate

**Return-type :** void

**Usage :** `spiSetClock(500000);`


### `spiSetDataMode()`

**Description :** Set SPI mode

**Parameters**


`mode` SPI mode (can be 0,1,2,3)

**Return-type :** void

**Usage :** `spiSetDataMode(0);`

### **Delay API**

### `delay()`

**Description :** Used to produce millisecond delays

**Parameters**


`howLong(unsigned int)` - Delay in ms to be produced

**Return-type :** void

**Usage :** `delay(50);`


**delayMicroseconds()**

**Description:** Used to produce microsecond delays

**Parameters**


`howLong(unsigned int)` - Delay in us to be produced

**Return-type:** void

**Usage:** `delayMicroseconds(50);`


### **UART API**

### `serialBegin()`

**Description:** Initialize the serial port

**Parameters**


- Bus Device node for Serial device driver
- baud Baudrate for Serial communication


**Return-type :** int

**Returns :** Return file descriptor on SUCCESS

**Usage :** `serialOpen("1",115200)`


### `serialEnd()`

**Description:** Release the serial port

**Return-type :** void

**Usage :** `serialEnd()`


### `serialRead()`

**Description :** Get a single character from the serial device

**Return-type :** int

**Returns:** Return the character on SUCCESS

**Usage :** `serialRead()`


### `serialPrint()`

**Description:** Print over Serial interface

**Parameters**


- message Message to be printed in serial

**Return-type :** void

**Usage :** `serialPrint("Hello World");`


### `serialPrintln()`

**Description:** Print over Serial interface with a new line

**Parameters**

- message Message to be printed in serial

**Return-type :** void

**Usage :** `serialPrintln("Hello World");`


### `serialWrite()`

**Description:** Send a single character to the serial port

**Parameters**


- c Character to be sent

**Return-type :** void

**Usage :** `serialWrite("I");`


### `serialAvailable()`

**Description:** Return the number of bytes of data available to be read in the serial port

**Return-type :** int

**Returns :** Return the number of bytes of data available to be read in the serial port

**Usage :** `serialAvailable()`


### `serialFlush()`

**Description:** Flush the serial buffers (both tx & rx)

**Return-type :** void

**Usage :** `serialFlush()`


### `Interrupt API`

**<a name="interrupt"/>`attachInterrupt()`</a>**

**Description:** Sets the digital value of the GPIO pin.

**Parameters**


- pin Physical pin number of the GPIO
- (*function) The function that will get the executed in the ISR
- mode Interrupt modes EDGE_FALLING or EDGE_RISING


**Return-type :** int

**Returns :** Return 0 on SUCCESS, Return -1 on Failure

**Usage :** `attachInterrupt(30, isr, INT_EDGE_BOTH);`


### **PWM API**

### `softPwmBegin()`

**Description:** Start the PWM at an initial duty cycle and defined Pulse width.

**Parameters**


- pin Physical pin number of the GPIO pin
- initialValue The initial duty cycle of the PWM signal in percentage.
- period The defined Pulse width in micro secs


**Return-type :** int

**Returns :** Return 0 on SUCCESS, Return -1 on Failure

**Usage :** `softPwmBegin(40, 50, 100);`


### `softPwmWrite()`

**Description:** Change the PWM duty cycle.

**Parameters**


- pin Physical pin number of the GPIO pin
- value PWM duty cycle in percentage.


**Return-type :** void

**Usage :** `softPwmWrite(40, 60);`


### `softPwmEnd()`

**Description:** Stop the PWM on a pin.

**Parameters**


- pin Physical pin number of the GPIO pin

**Return-type :** void

**Usage :** `softPwmEnd(40);`
