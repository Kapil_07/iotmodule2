# **3.1 Sensors and Actuators**

![](https://gitlab.com/Kapil_07/iotmodule2/-/raw/master/assignments/summary/Summary1/images/Capture.PNG)

#         **Sensors**

![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/top_sensor_types_used_in_iot-04.png)


#           **Actuators**

![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/top_sensor_types_used_in_iot-05.png)


# **3.2 Analog and Digital**

![](https://2.bp.blogspot.com/-PF2bT1_chTA/UWMxi0ALH_I/AAAAAAAAEKI/YzVKKYqrSlY/s1600/ADC-DAC.png)

# **3.3 Micro controllers vs Micro processors** 

![](https://pediaa.com/wp-content/uploads/2018/07/Difference-Between-Microprocessor-and-Microcontroller-Comparison-Summary.jpg)

# **3.4 Introduction to Raspberry Pi 3**
![](https://www.silverlineelectronics.in/pub/media/catalog/product/cache/e4d64343b1bc593f1c5348fe05efa4a6/r/a/raspberry_pi_3.jpg)

# **What are interfaces ?**

## Interfaces are way to connect sensors/actuators to a micro processor.

### **Raspberry Pi Interfaces**

- GPIO
- I2C
- SPI
- UART
- PWM

# **Serial and Parallel**

![](https://miro.medium.com/max/1103/0*Hi44DwHAAkm7Jh_q.gif)

## **Serial Interfaces**
- I2C
- SPI
- UART

## **Parallel Interfaces**
- GPIO
