# **4.2 Modbus**

# **Modbus Communication Protocol**

- Modbus is a communication protocol for use with programmable logic controllers (PLC).
- It is typically used to transmit signals from instrumentation and control devices back to a main controller, or data gathering system.
- The device requesting information is called “master” and “slaves” are the devices supplying information.
- In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.
- Communication between a master and a slave occurs in a frame that indicates a function code.
- The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.
- The slave then responds, based on the function code received.
- The protocol is commonly used in IoT as a local interface to manage devices.

- Modbus protocol can be used over 2 interfaces
- RS485 - called as Modbus RTU
- Ethernet - called as Modbus TCP/IP

# **4.3 OPCUA**

![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/5919e2325a45423a158c276511dde466/opcua1.png)

![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/d24af13157b45bd8102a6d4e278a007f/opcua2.png) ![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/79e68efc4d1b12853622b04795a271f9/opcua3.png)


# **5.1 MQTT**

- **MQTT** (Message Queuing Telemetry Transport) is a simple messaging protocol designed for constrained devices with low bandwidth.
- It uses a publish/subscribe communication pattern, for machine-to-machine (M2M) communication.
- A perfect solution for IOT applications, also allows to send commands to control outputs, read and publish data from sensor nodes.
- Communication between several devices can be established simultaneously.

## How MQTT Works

![](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/358848b4b9222917e31db143ff0a6383/Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol.png)

MQTT Client as a publisher sends a message to the MQTT broker whose work is to distribute the message accordingly to all other MQTT clients subscribed to the topic on which publisher publishes the message.
Topics are a way to register interest for incoming messages or to specify where to publish the message. Represented by strings, separated by forward slash. Each slash indicates a topic level.
Clients can subscribe to a specific level of a topic's hierarchy or use a wildcard character to subscribe to multiple levels.

# **5.2 HTTP**

## Hyper Text Transport Protocol

![](https://bytesofgigabytes.com/IMAGES/Networking/HTTPcommuncation/http%20communication.png)

## The request

### Request has 3 parts

- Request line
- HTTP headrs
- message body

**GET request**
it is a type of HTTP request using the GET method

```GET / HTTP/1.1
Host: xkcd.com
Accept: text/html
User-Agent: Mozilla/5.0 (Macintosh)
```

There are different types of methods

**GET**

  Retrieve the resource from the server (e.g. when visiting a page);

**POST**

  Create a resource on the server (e.g. when submitting a form);

**PUT/PATCH**

  Update the resource on the server (used by APIs);

**DELETE**

  Delete the resource from the server (used by APIs).

## The response

response is made of 3 parts

- Status line
- HTTP header
- Message body

```HTTP/1.1 200 OK
Date: Sat, 02 Apr 2011 21:05:05 GMT
Server: lighttpd/1.4.19
Content-Type: text/html

<html>
    <!-- ... HTML for the xkcd comic -->
</html>
```
**The server will send you different status codes depending on situation**

![](https://i1.wp.com/csharpcorner.mindcrackerinc.netdna-cdn.com/article/create-api-with-asp-net-core-day-three-working-with-http-status-codes-in-asp/Images/image002.jpg)

![](https://i.ytimg.com/vi/Lz0dy9uhD1I/maxresdefault.jpg)
